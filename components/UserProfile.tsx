// UI component for user profile
import Image from "next/image";

export default function UserProfile({ user }) {
  return (
    <div className="box-center">
      {/* <img src={user.photoURL || "/hacker.png"} className="card-img-center" /> */}
      <div className="card-img-center">
        <Image
          src={user.photoURL || "/hacker.png"}
          alt={user.username}
          height={96}
          width={96}
        />
      </div>
      <p>
        <i>@{user.username}</i>
      </p>
      <h1>{user.displayName || "Anonymous User"}</h1>
    </div>
  );
}
